import os
import numpy as np
import matplotlib.pyplot as plt
from system import IMAGEPATH as IMP

notes = ['do', 're', 'mi', 'fa(fa#)', 'sol', 'la', 'si']
nat = [ 1, 9/8, 5/4, 4/3, 3/2, 13/8, 15/8 ]
pit = [ 1, 9/8, 81/64, 729/512, 3/2, 27/16, 243/128 ]
semis = [ 0, 2, 4, 5, 7, 9, 11 ]
temp = [2**(n/12) for n in semis]

# for scale in [nat, pit, temp]:
#     for n in scale:
#         print("%f " % (n), end='')
#     print()

x_axis = np.arange(len(nat))

fig = plt.figure(figsize=(3.5,4), dpi=150)
plt.xticks(x_axis, notes)
plt.bar(x_axis-0.3, nat, width=0.3)
plt.bar(x_axis+0.0, pit, width=0.3)
plt.bar(x_axis+0.3, temp, width=0.3)
plt.axis([-0.6, 7, 0.9, 2])
fig.tight_layout()

plt.savefig(os.path.join(IMP, 'scales.png'))
