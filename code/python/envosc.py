import os
import numpy as np
import matplotlib.pyplot as plt
from system import IMAGEPATH as IMP

sr = 2000
sinc = 1/2000
dur = 1


t = np.linspace(0, dur, dur*sr)
freq = 15
amp  = np.exp(-10.0*t)
y = amp*np.cos(2*np.pi*freq*t)

fig = plt.figure(figsize=(3.5,4), dpi=150)
plt.plot(t, y)
plt.plot(t, amp)
plt.axis([0, dur, -1, 1])
fig.tight_layout()

plt.savefig(os.path.join(IMP, 'envosc.png'))
