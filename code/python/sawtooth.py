import os
import numpy as np
import matplotlib.pyplot as plt
from system import IMAGEPATH as IMP

sr = 2000
sinc = 1/2000
dur = 1


t = np.linspace(0, dur, dur*sr)
freq = 15
amp  = 0.6
y = amp*np.cos(2*np.pi*freq*t)
nharms = 10
saw = np.zeros(np.size(t))

fig = plt.figure(figsize=(3.5,4), dpi=150)
plt.subplot(211)
for num in range(nharms):
    n = num+1
    y = amp*(1/n)*np.sin(2*np.pi*freq*n*t)
    plt.plot(t, y)
    saw += y
plt.axis([0, 0.2, -1, 1])
fig.tight_layout()
plt.subplot(212)
plt.plot(t, saw)
plt.axis([0, 0.2, -1, 1])
fig.tight_layout()

plt.savefig(os.path.join(IMP, 'sawtooth.png'))
