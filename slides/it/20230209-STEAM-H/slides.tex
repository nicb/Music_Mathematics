%
% Copyright (C) 2018 Nicola Bernardini nicb@sme-ccppd.org
% 
% This work is licensed under a Creative Commons License, and specifically the
% 
%   Creative Commons Attribution-ShareAlike 4.0 License
%   http://creativecommons.org/licenses/by-sa/4.0/legalcode
% 
%

\newcommand{\mytop}{../../..}
\newcommand{\mylang}{it}
\newcommand{\imagedir}{\mytop/slides/images}
\newcommand{\exampledir}{\mytop/examples}
\newcommand{\templatedir}{\mytop/../latex/beamer/smerm}
\newcommand{\inputfrom}[2]{\input{#1/#2}}
\documentclass[compress,\printingmode]{beamer}

\usepackage{beamerthemeSMERM}
\usepackage{beamercolorthemeSMERM}
\usepackage{beamerinnerthemeSMERM}

\usepackage{colortbl}
\usepackage[italian]{babel}
\usepackage{pgf}
\usepackage{xspace}

\usepackage{multimedia}
\usepackage{xmpmulti}
\usepackage{hyperref}
\usepackage{gitinfo2}
\newcommand{\rcstag}{rev.\gitAbbrevHash\ \ \gitAuthorDate\xspace}
\usepackage{gensymb}

\newcommand{\cpyear}{2023}
\newcommand{\cpholder}{Nicola Bernardini}
\newcommand{\cpholderemail}{n.bernardini@conservatoriosantacecilia.it}

% Use some nice templates

%\beamertemplateshadingbackground{red!10}{structure!10}
\beamertemplatetransparentcovereddynamic
\beamertemplateballitem
\beamertemplatenumberedballsectiontoc

% My colors
\definecolor{notdone}{gray}{0.35}
\definecolor{reiterate}{rgb}{0.5882 0.3372 0}

%\usecolortheme[named=MyColor]{structure}
%\usecolortheme[named=MyColor]{structure}
\beamertemplateshadingbackground{white!10}{white!10}

\input{\templatedir/macros}
\setbeamerfont{smalllist}{size=\scriptsize}

\title[Musica/Matematica]
{%
  Informatizzazione dei dati:\\
  Matematica e Musica (elettronica)\\
  {\tiny (\rcstag)}
}

\author{%
  Nicola Bernardini\\
    \href{mailto:\cpholderemail}{\cpholderemail}
}
\institute[SMERM]%
{%
  \href{http://www.conservatoriosantacecilia.it}
     {Conservatorio di Musica ``Santa Cecilia'' -- Roma}
}
\date[STEAM-H 09/02/2023]{Well within STEAM - Roma 9 febbraio 2023}

\begin{document}
\newcounter{ms}
\setcounter{ms}{0}
  
\begin{frame}
  \titlepage
\end{frame}

\section{Introduzione}

\begin{frame}

   \frametitle<+->{Di cosa parleremo}

   \begin{itemize}[<+- | alert@+>]

      \item La relazione tra matematica e musica

         \begin{itemize}[<+- | alert@+>]

            \item i dati che diventano \emph{suoni}

            \item i suoni che diventano musica

         \end{itemize}

       \item Dai suoni alla musica

       \item L'apporto delle tecnologie elettroniche

       \item Il software per il suono e per la musica

%       \item Musica e Big Data

   \end{itemize}

\end{frame}

\section{Matematica e Musica}

\subsection{Dati -> Suoni}

\begin{frame}
    \frametitle<+->{Numeri e suoni}

   \begin{itemize}[<+- | alert@+>]

      \item La relazione tra numeri e suoni \`e nota sin dai tempi dei greci

      \item Essa deriva dalla pseudo--periodicit\`a dei suoni\ldots

      \item \ldots che sono rapide e microscopiche variazioni di pressione
            dell'aria rilevate da uno strumentario di alta precisione:

      \item le nostre orecchie

   \end{itemize}

\end{frame}

\begin{frame}
   \frametitle<+->{Il suono pi\`u semplice}

   \begin{columns}

     \begin{column}[T]{0.5\textwidth}

   \begin{itemize}[<+- | alert@+>]
   \usebeamerfont{smalllist}

        \item il suono pi\`u semplice corrisponde all'oscillazione pi\`u
                semplice:

        \item un'onda sinusoidale, ossia una funzione sinusoidale di un angolo
                che varia nel tempo a velocit\`a diverse, ossia

        \item $y = A~sin(2 \pi f t)$

        \item dove: 

             \begin{itemize}[<+- | alert@+>]
             \usebeamerfont{smalllist}

                \item $A$: ampiezza del suono

                \item $f$: frequenza del suono

                \item $t$: tempo

             \end{itemize}

   \end{itemize}

     \end{column}
     \begin{column}[T]{0.5\textwidth}
        \pgfimage[width=\columnwidth]{\imagedir/osc}
     \end{column}

   \end{columns}

\end{frame}

\begin{frame}
   \frametitle<+->{Qualcosa di pi\`u musicale}

   \begin{columns}

     \begin{column}[T]{0.5\textwidth}

   \begin{itemize}[<+- | alert@+>]
   \usebeamerfont{smalllist}

        \item Le onde sinusoidali sono troppo semplici per la musica

        \item aggiungendo un \emph{inviluppo} si ottiene gi\`a qualcosa di
                pi\`u ``musicale''

        \item $y = A(t)~sin(2 \pi f t)$

        \item dove: 

             \begin{itemize}[<+- | alert@+>]
             \usebeamerfont{smalllist}

                \item (per esempio) $A(t) = e^{at}$

             \end{itemize}

   \end{itemize}
   \end{column}
     \begin{column}[T]{0.5\textwidth}
        \pgfimage[width=\columnwidth]{\imagedir/envosc}
     \end{column}

   \end{columns}

\end{frame}

\begin{frame}
   \frametitle<+->{Ancora di pi\`u: la forma d'onda}

   \begin{columns}

     \begin{column}[T]{0.5\textwidth}

   \begin{itemize}[<+- | alert@+>]
   \usebeamerfont{smalllist}

        \item Sommando onde sinusoidali \emph{armoniche} (ossia a frequenze
              multiple della fondamentale) \ldots

        \item \ldots si ottengono forme d'onda diverse a seconda delle diverse
                funzioni di riscalamento delle ampiezze di ciascuna componente

        \item $y = \sum_{k = 1}^{N}{A(k)~sin(2 \pi f k t)}$

        \item Questa tecnica si chiama \emph{sintesi additiva}

        \item e si basa sulla trasformata inversa di Fourier

        \item La forma d'onda fornisce uno dei contributi pi\`u grandi a ci\`o
              che noi percepiamo come \emph{timbro} di un suono

   \end{itemize}
   \end{column}
     \begin{column}[T]{0.5\textwidth}
        \pgfimage[width=\columnwidth]{\imagedir/sawtooth}
     \end{column}

   \end{columns}

\end{frame}

\begin{frame}
     \frametitle<+->{Infine: i suoni musicali}
  
     \begin{itemize}[<+- | alert@+>]
  
        \item Naturalmente, i suoni musicali sono molto di pi\`u di tutto
                questo:
  
        \item tutto cambia nel tempo -- la frequenza, l'ampiezza, il timbro
  
        \item I suoni musicali sono quindi la risultante di numerose modulazioni
                temporali di tutti i parametri in gioco (funzioni di funzioni)
  
        \item In tutto questo, le nostre orecchie e il nostro cervello giocano
              un ruolo essenziale: la \emph{fusione spettrale}

     \end{itemize}
  
\end{frame}

\subsection[Suoni -> Musica]{Dal Suono alla Musica}

\begin{frame}
     \frametitle<+->{E la musica?}

     \begin{itemize}[<+- | alert@+>]

        \item La musica \`e l'organizzazione dei dati numerici riguardanti i
              suoni secondo strutture e schemi diversificati

        \item In buona sostanza, si tratta di \emph{segmentare} spazi continui
              (le frequenze, il tempo, le durate, le ampiezze, i timbri, ecc.)

        \item \`E dalla segmentazione (e dalla conseguente organizzazione
                degli elementi) che prolifera il \emph{senso musicale}

     \end{itemize}
  
\end{frame}

\setcounter{ms}{0}
\refstepcounter{ms}
\begin{frame}
   \frametitle<+->{Esempi di segmentazione (\arabic{ms}): le scale musicali}
   \begin{columns}

     \begin{column}[T]{0.5\textwidth}

   \begin{itemize}[<+- | alert@+>]
   \usebeamerfont{smalllist}

      \item la scala naturale: le componenti armoniche riportate all'interno
            di un'ottava ($1$, $\frac{9}{8}$, $\frac{5}{4}$, $\frac{4}{3}$,
            $\frac{3}{2}$, $\frac{13}{8}$, $\frac{15}{8}$) 

      \item la scala pitagorica: intonazione per quinte successive
            ($f_{n+1} = f_{n} \times \frac{3}{2}$: $1$, $\frac{9}{8}$, $\frac{81}{64}$, $\frac{729}{512}$,  $\frac{3}{2}$, $\frac{27}{16}$, $\frac{243}{128}$)

      \item la scala temperata: progressione geometrica
            ($f_{n+1} = f_{n} \times 2^{\frac{1}{12}}$: $1$,
            $2^{\frac{1}{6}}$, $2^{\frac{1}{4}}$, $2^{\frac{5}{12}}$,
            $2^{\frac{7}{12}}$, $2^{\frac{3}{4}}$, $2^{\frac{11}{12}}$)

      \item altre scale (pentatoniche, $f_{n+1} = f_{n} \times 5^{\frac{1}{25}}$, ecc.)

   \end{itemize}
   \end{column}
     \begin{column}[T]{0.5\textwidth}
        \pgfimage[width=\columnwidth]{\imagedir/scales}
     \end{column}

   \end{columns}

\end{frame}

\refstepcounter{ms}
\begin{frame}
   \frametitle<+->{Esempi di segmentazione (\arabic{ms}): il ritmo}
%  \begin{columns}

%    \begin{column}[T]{0.5\textwidth}

   \begin{itemize}[<+- | alert@+>]
%  \usebeamerfont{smalllist}

      \item La segmentazione del tempo \`e ci\`o che noi chiamiamo \emph{il ritmo}

      \item il ritmo \`e la combinazione di pulsazione, accenti e
            raggruppamento degli accenti (il \emph{metro})

      \item la sovrapposizione di questi elementi d\`a luogo a
              \emph{poliritmie}, \emph{polimetrie}, ecc.

   \end{itemize}
%  \end{column}
%    \begin{column}[T]{0.5\textwidth}
%    \end{column}

%  \end{columns}

\end{frame}

\refstepcounter{ms}
\begin{frame}
   \frametitle<+->{Esempi di segmentazione (\arabic{ms}): i timbri}
%  \begin{columns}

%    \begin{column}[T]{0.5\textwidth}

   \begin{itemize}[<+- | alert@+>]
%  \usebeamerfont{smalllist}

      \item La segmentazione del timbro avviene attraverso gli \emph{strumenti
              musicali}

      \item Tradizionalmente, gli strumenti musicali sono divisi in gruppi
              (le \emph{voci}, gli \emph{archi}, i \emph{legni}, gli
              \emph{ottoni}, le \emph{percussioni}, ecc.) e poi in famiglie

      \item Nella musica pi\`u recente gli strumenti vengono anche raggruppati
              per \emph{famiglie acustiche} (cf. Berio)

   \end{itemize}
%  \end{column}
%    \begin{column}[T]{0.5\textwidth}
%    \end{column}

%  \end{columns}

\end{frame}

\section{La Natura della Musica}

\setcounter{ms}{0}
\refstepcounter{ms}
\begin{frame}
   \frametitle<+->{La natura della Musica (\arabic{ms})}

   \begin{itemize}[<+- | alert@+>]

      \item Naturalmente, la musica non \`e soltanto la segmentazione di spazi
              dimensionali continui

      \item Una volta scelte tali segmentazioni, \`e necessario organizzarle

      \item L'organizzazione pu\`o apparire intuitiva e talvolta (anche se
              raramente) lo \`e \ldots

      \item \ldots In realt\`a, essa segue logiche abbastanza precise e
              definite che mutano con le epoche storiche

      \item Queste logiche dipendono, oltre che dal contesto storico, dalle
              \emph{funzioni musicali} (ossia: dal \emph{perch\`e} certa
              musica \`e stata scritta)

   \end{itemize}

\end{frame}

\refstepcounter{ms}
\begin{frame}
   \frametitle<+->{La natura della Musica (\arabic{ms}): le funzioni}

   \begin{itemize}[<+- | alert@+>]

       \item Gli etnomusicologi identificano tre funzioni primarie della
               creazione musicale:

          \begin{enumerate}[<+- | alert@+>]

              \item La funzione \emph{rituale}

              \item La funzione \emph{ludica} (o \emph{di intrattenimento})

              \item La funzione \emph{speculativa} (o \emph{conoscitiva})

          \end{enumerate}

       \item In linea di principio, un brano musicale contiene tutte queste
             funzioni, seppure in diverse gradazioni

       \item Oggi, la funzione \emph{ludica} \`e assolutamente dominante
             rispetto alle altre

       \item Tuttavia, la dominazione della funzione \emph{ludica} ci fa
               perdere alcune capacit\`a di \emph{ascolto}

   \end{itemize}

\end{frame}

\refstepcounter{ms}
\begin{frame}
   \frametitle<+->{La natura della Musica (\arabic{ms}): come funziona?}

   \begin{itemize}[<+- | alert@+>]

    \item Le regole della composizione musicale sono sempre state dettate
            dalle \emph{funzioni} combinate con l'ambito sociale di
            riferimento

    \item La fluidificazione del contesto sociale ha prodotto una estensione
          combinatoria delle regole compositive sino a rendere plausibile e
          sensata qualsiasi scelta musicale

    \item Rimane per\`o un elemento intrinseco e imprescindibile:

    \item la Musica \`e un'arte del tempo, e pertanto rimane soggiogata alle
            leggi termodinamiche dell'informazione

    \item Oggi, comporre (per qualsiasi motivo o funzione), significa sempre
          equilibrare elementi \emph{ridondanti} con elementi di
          \emph{novit\`a}

   \end{itemize}

\end{frame}

\refstepcounter{ms}
\begin{frame}
   \frametitle<+->{La natura della Musica (\arabic{ms}): i \emph{capolavori}}
   \begin{columns}

     \begin{column}[T]{0.5\textwidth}

   \begin{itemize}[<+- | alert@+>]
   \usebeamerfont{smalllist}

      \item I \emph{capolavori} sono quindi delle opere in cui questo
              equilibrio viene rispettato anche in condizioni estremamente
              sofisticate

      \item Un esempio per tutti: il secondo movimento (\emph{Adagio}) \emph{Concerto per Violino} op.77 di
              Johannes Brahms 

      \item Brahms \`e il maestro assoluto del principio della \emph{variazione continua}

      \item \url{https://youtu.be/JWGrZgRf8wo}

   \end{itemize}
   \end{column}
     \begin{column}[T]{0.5\textwidth}
        \pgfimage[width=\columnwidth]{\imagedir/johannes_brahms}
     \end{column}

   \end{columns}

\end{frame}

\section{Tecnologie Elettroniche}

\setcounter{ms}{0}
\refstepcounter{ms}
\begin{frame}
   \frametitle<+->{Le tecnologie elettroniche (\arabic{ms})}
   \begin{columns}

     \begin{column}[T]{0.5\textwidth}

   \begin{itemize}[<+- | alert@+>]
   \usebeamerfont{smalllist}

      \item La seconda guerra mondiale produce un intenso sviluppo delle
              tecnologie elettroniche e informatiche

      \item I compositori si trovano all'interno delle stazioni radiofoniche e
              cominciano a sperimentare con le apparecchiature presenti
              (oscillatori, generatori di funzioni, filtri, ecc.)

      \item Il terreno \`e fertile per liberarsi dalle gabbie delle
              segmentazioni sedimentate nel tempo

      \item Nasce la \emph{musica elettronica}

   \end{itemize}
   \end{column}
     \begin{column}[T]{0.5\textwidth}
        \pgfimage[width=\columnwidth]{\imagedir/studio_fonologia_rai}
     \end{column}

   \end{columns}

\end{frame}

\refstepcounter{ms}
\begin{frame}
   \frametitle<+->{Le tecnologie elettroniche (\arabic{ms})}
   \begin{columns}

     \begin{column}[T]{0.5\textwidth}
   \begin{itemize}[<+- | alert@+>]
   \usebeamerfont{smalllist}

      \item A partire dagli anni '60, la musica di consumo
           si appropria delle tecnologie elettroniche \ldots

      \item \ldots e  partire dagli anni '80, essa
           si appropria anche delle tecnologie digitali

      \item Al contrario della ricerca compositiva degli anni '50,
           la musica di consumo utilizza queste tecnologie per surrogare le
           segmentazioni precedenti \ldots

      \item \ldots e per rendere pi\`u semplice ed efficace (ma anche pi\`u
            stereotipica) la produzione della musica

   \end{itemize}
   \end{column}
     \begin{column}[T]{0.5\textwidth}
        \pgfimage[width=\columnwidth]{\imagedir/commercial_electronic}
     \end{column}

   \end{columns}

\end{frame}

\refstepcounter{ms}
\begin{frame}
   \frametitle<+->{Le tecnologie elettroniche (\arabic{ms})}
   \begin{columns}[T]

     \begin{column}{0.5\textwidth}
   \begin{itemize}[<+- | alert@+>]
   \usebeamerfont{smalllist}

      \item La ricerca in questo ambito per\`o continua e si \`e molto estesa e
            diversificata

      \item Ormai non si parla pi\`u di \emph{musica elettronica} quanto di
            \emph{Sound and Music Computing}

      \item Nella societ\`a dell'informazione, il Sound \& Music Computing
            svolge funzioni primarie (\emph{information retrieval}, \emph{data mining},
            \emph{computer listening}, ecc.) \ldots

      \item \ldots oltre alle attivit\`a precedenti (la composizione,
              l'esecuzione, il \emph{live--electronics}, ecc.)

   \end{itemize}
   \end{column}
     \begin{column}{0.5\textwidth}
        \begin{center}
        \pgfimage[height=0.7\textheight]{\imagedir/Sound_and_Music_Computing}
        \end{center}
     \end{column}

   \end{columns}

\end{frame}

\section{Il Software}

\setcounter{ms}{0}
\refstepcounter{ms}
\begin{frame}
   \frametitle<+->{Il software (\arabic{ms})}
   \begin{columns}

     \begin{column}[T]{0.5\textwidth}
   \begin{itemize}[<+- | alert@+>]
   \usebeamerfont{smalllist}

        \item Dagli anni '50 in poi inizia un febbrile sviluppo di software
              dedicato alla creazione di suoni e di musica

        \item Nell'ambito della ricerca, questi software cercano di
                semplificare alcune operazioni ricorrenti (oscillatori,
                filtri, ecc.) senza cercare di interpretare i desiderata dei
                compositori

        \item In ambito di consumo si cerca invece di capire quali siano le
              operazioni \emph{musicali} pi\`u comuni producendo applicazioni
              che stereotipizzano la creazione musicale

   \end{itemize}
   \end{column}
     \begin{column}[T]{0.5\textwidth}
        \pgfimage[width=\columnwidth]{\imagedir/flstudio}
     \end{column}

   \end{columns}

\end{frame}

\refstepcounter{ms}
\begin{frame}
   \frametitle<+->{Il software (\arabic{ms}): il \emph{Software Libero}}

   \begin{itemize}[<+- | alert@+>]

      \item Grazie al cielo, negli anni '80 si sviluppa il movimento del
              \emph{Software Libero}

       \item Il Software Libero \`e software protetto da licenze che
               garantiscono agli utenti quattro diritti fondamentali:

        \begin{itemize}[<+- | alert@+>]

           \item il \emph{diritto di esecuzione}

           \item il \emph{diritto di studio} del codice sorgente

           \item il \emph{diritto di copiare} il codice sorgente

           \item il \emph{diritto di condividere} il codice propagando gli
                   stessi diritti ai destinatari della condivisione \ldots

        \end{itemize}

       \item \ldots il che implica che \`e impossibile combinare insieme
              Software Libero e software non--libero (proprietario) (licenze
              virali)


   \end{itemize}

\end{frame}

\refstepcounter{ms}
\begin{frame}
   \frametitle<+->{Il software (\arabic{ms}): il \emph{Software Libero}}

   \begin{itemize}[<+- | alert@+>]

      \item \`E di cruciale importanza notare che:

        \begin{itemize}[<+- | alert@+>]

       \item Il software \emph{open--source} {\bfseries\emph{NON}} \`e
               (automaticamente) Software Libero

       \item Il Software Libero {\bfseries\emph{NON}} \`e (automaticamente)
             gratis

        \end{itemize}

   \end{itemize}

\end{frame}

\refstepcounter{ms}
\begin{frame}
   \frametitle<+->{Il software (\arabic{ms}): il \emph{Software Libero} e la musica}

   \begin{itemize}[<+- | alert@+>]

      \item il Software Libero gioca un ruolo importantissimo nelle arti
              creative

      \item Esso garantisce l'indipendenza della creativit\`a

      \item Fortunatamente, molto Software Libero \`e dedicato alla musica

      \item L'unico problema \`e conoscerne l'esistenza \ldots

      \item \ldots perch\'e la distribuzione del Software Libero \`e
            contro--intuitiva in una logica di mercato

   \end{itemize}

\end{frame}

\subsection{\texttt{Pure Data}}

\setcounter{ms}{0}
\refstepcounter{ms}
\begin{frame}
   \frametitle<+->{Un esempio: \emph{Pure Data}}
   \begin{columns}

   \begin{column}[T]{0.5\textwidth}
   \begin{itemize}[<+- | alert@+>]
   \usebeamerfont{smalllist}

    \item \emph{Pure Data} \`e un linguaggio di programmazione grafico
            pensato per la \emph{prototipazione veloce} di strumenti musicali
            elettronici

    \item Sviluppato dagli anni '90 in poi dal matematico/musicista Miller
            Puckette all'universit\`a UCSD San Diego

    \item Essendo Software Libero, \emph{Pure Data} \`e attualmente sviluppato
          da migliaia di musicisti che collaborano con Puckette

   \end{itemize}
   \end{column}
     \begin{column}[T]{0.5\textwidth}
        \pgfimage[width=\columnwidth]{\imagedir/pure_data}
     \end{column}

   \end{columns}

\end{frame}
\subsection{Altri}

\setcounter{ms}{0}
\refstepcounter{ms}
\begin{frame}
   \frametitle<+->{Altri esempi}
   \begin{columns}

   \begin{column}[T]{0.5\textwidth}
   \begin{itemize}[<+- | alert@+>]
   \usebeamerfont{smalllist}

    \item Molti altri software protetti da licenze libere sono disponibili per
            la creazione musicale:

      \begin{itemize}[<+- | alert@+>]
      \usebeamerfont{smalllist}

        \item \emph{csound} per la composizione musicale in tempo differito

        \item \emph{SuperCollider} per la composizione algoritmica e il
                \emph{live--coding}

        \item \emph{lilypond} per la tipografia musicale

        \item \emph{faust} per la creazione di algoritmi di elaborazione del suono

        \item \emph{sound editors}, \emph{digital patchers}, ecc.

      \end{itemize}

   \end{itemize}
   \end{column}
     \begin{column}[T]{0.5\textwidth}
        \pgfimage[width=\columnwidth]{\imagedir/software}
     \end{column}

   \end{columns}

\end{frame}
% \section[Big Data]{Musica e Big Data}

\section{Sorgenti}

\begin{frame}

   Le sorgenti di questa presentazione (con tutto il codice e i materiali
   connessi) si trovano qui:\\[2\baselineskip]

   \begin{center}
      \url{https://gitlab.com/nicb/Music_Mathematics}
   \end{center}

\end{frame}

\end{document}
