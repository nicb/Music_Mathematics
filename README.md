# (ELECTRONIC) MUSIC AND MATHEMATICS

![CC BY-SA](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)

(currently this lecture is exclusively in italian)

This lecture outlines the existing relationship between numeric data,
mathematics and music. Starting from very simple oscillatory phenomena,
the full gamut ranging from sound to music is explored. After that,
the current music technology to study and create sound is explored, with a
particular focus on Free Software and `pure data`.

# DELIVERED

1. STEAM-H Project - Marymount College Rome Italy 10/02/2023

# LICENSE

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img
alt="Creative Commons License" style="border-width:0"
src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This
work is licensed under a <a rel="license"
href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons
Attribution-ShareAlike 4.0 International License</a>.

Please read the [License](./LICENSE.md) for more information.
